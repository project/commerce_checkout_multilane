<?php
/**
 * @file
 * Enable/disable payment methods on a checkout lane basis.
 */

/**
 * Implements hook_default_rules_configuration_alter().
 */
function commerce_checkout_multilane_payment_default_rules_configuration_alter(&$configs) {
  foreach (commerce_checkout_multilane_lanes() as $lane_id => $lane) {
    if (variable_get('commerce_checkout_multilane_payment_enabled_' . $lane_id)) {
      $whitelist = variable_get('commerce_checkout_multilane_payment_whitelist_' . $lane_id, array());

      foreach (commerce_payment_methods() as $payment_method) {
        $method_id = $payment_method['method_id'];
        $rule_name = 'commerce_payment_' . $method_id;
        if (empty($whitelist[$method_id]) && isset($configs[$rule_name])) {
          $condition = rules_condition('data_is', array(
            'data:select' => 'commerce-order:checkout-lane-id',
            'value' => $lane_id
          ));
          $condition->negate();
          $configs[$rule_name]->condition($condition);
        }
      }
    }
  }
}
